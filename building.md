# Building artefacts

- Font
- Specimen

## Font

Build an OTF in Glyphs in the usual way.


## Specimen

- Edit an HTML document in TextEdit.app
- Open in Firefox
- Print to PDF with A4 page size and 0 margin
- Open in Preview and Export to PNG using 38.4 pixels/cm
- Check PNG is 806 pixels wide and open in Preview
- Crop to 806 wide and 1116 high, Command-C, Command-N
- Save that new PNG.

or

use the GitLab Continuous Integration.
The `.gitlab-ci.yml` file creates a GitLab pipeline action
that, if successful, creates a PDF and a PNG file in a zip file
that you can download from the [pipeline page](-/pipelines).
This uses `fixfontface` to add CSS at-rules to point to the
current font file, and
`wkhtmltopdf` to render HTML to PDF and PNG.

It's a modern miracle.


## Specimen Technical Notes

Refer to [Mozilla Developer Network Web Fonts learning
module](https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_text/Web_fonts).
A brief recap: `font:` property specifies name of font family,
which is either an installed font, or (and this is better)
associated with an arbitrary font-file via a `@font-face` CSS
declaration.

HTML already has

    font: 60.0px     'Avimode LAB 20211125'

So we need to add a @font-face rule:

    @font-face {
      font-family: "Avimode";
      src: url("AvimodeLAB20220209-Regular.otf");
    }

This actually seems to work.

Note that the font-family in the paragraph style is put there by
TextEdit.app and is the name of the font as it is installed on macOS.
When rendered as HTML, the actual font-file used can be
retargeted using a `@font-face` declaration.

# END
